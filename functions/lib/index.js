"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
admin.initializeApp();
exports.onUserCreation = functions.auth.user().onCreate((event) => {
    event.displayName = "New User";
    return admin.firestore().collection(`users`).doc(event.uid).set({ active: true, role: 'user' });
});
exports.onUserDeletion = functions.auth.user().onDelete((event) => {
    return admin.firestore().collection(`users`).doc(event.uid).delete();
});
exports.createUserGroup = functions.https.onRequest((request, response) => {
    admin.auth().verifyIdToken(request.header['authorization']).then(token => {
        const user = admin.auth().getUser(token.uid);
        const doc = admin.firestore().collection(`usergroups`).add({ test: 'group' });
    }).catch(err => {
        response.body.message = 'oh no';
        response.body.description = 'Auth failed.';
        response.statusCode = 401;
        response.send();
    });
});
//# sourceMappingURL=index.js.map