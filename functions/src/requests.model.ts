import * as functions from 'firebase-functions';

export interface ICreateUserGroupRequest extends functions.Request {
    body: {
        groupName: string;
    };
}