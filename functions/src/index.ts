import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as requestModels from './requests.model';
import * as responseModels from './responses.model';

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

admin.initializeApp();

export const onUserCreation = functions.auth.user().onCreate((event) => {
    event.displayName = "New User";
    return admin.firestore().collection(`users`).doc(event.uid).set({ active: true, role: 'user' });
});

export const onUserDeletion = functions.auth.user().onDelete((event) => {
    return admin.firestore().collection(`users`).doc(event.uid).delete();
})


export const createUserGroup = functions.https.onRequest((request: requestModels.ICreateUserGroupRequest, response: responseModels.IErrorResponse) => {
    admin.auth().verifyIdToken(request.header['authorization']).then(token => {
        const user = admin.auth().getUser(token.uid);
        const doc = admin.firestore().collection(`usergroups`).add({ test: 'group' });
    }).catch(err => {
        response.body.message = 'oh no';
        response.body.description = 'Auth failed.';
        response.statusCode = 401;
        response.send();
    });


});

