import * as functions from 'firebase-functions';

export interface IErrorResponse extends functions.Response {
    body?: {
        message?: string;
        description?: string;
    }
}