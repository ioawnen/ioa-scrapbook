export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAho19WySH-zuApxbchvIsHPV4ZXP8Wgds',
    authDomain: 'ioa-scrapbook-dev.firebaseapp.com',
    databaseURL: 'https://ioa-scrapbook-dev.firebaseio.com',
    projectId: 'ioa-scrapbook-dev',
    storageBucket: 'ioa-scrapbook-dev.appspot.com',
    messagingSenderId: '173230625374'
  }
};

/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
