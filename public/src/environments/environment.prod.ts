export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyBzJoLONYfhSGtn646VONvjaeE_n7p_wNg',
    authDomain: 'ioa-scrapbook.firebaseapp.com',
    databaseURL: 'https://ioa-scrapbook.firebaseio.com',
    projectId: 'ioa-scrapbook',
    storageBucket: 'ioa-scrapbook.appspot.com',
    messagingSenderId: '968602430930'
  }
};
