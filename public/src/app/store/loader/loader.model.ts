export interface ILoaderProcess {
    loading: boolean;
    mode: string;
    lastUpdated: Date;
}

export class LoaderStateModel {
    processes: Map<string, ILoaderProcess>;

    /** Loader properties. These shouldn't be updated directly! */
    visible: boolean;
    mode: string;
}
