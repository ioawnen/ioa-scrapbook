import { ILoaderProcess } from './loader.model';

export class UpdateLoadingStatus {
  static readonly type = '[Loader] Update Loading Status';

  public readonly optsDefaults: ILoaderProcess = {
    mode: 'NOT IMPLEMENTED',
    loading: false,
    lastUpdated: null
  };

  constructor(public processId: string, public opts?: Partial<ILoaderProcess>) {
    this.opts = { ...this.optsDefaults, ...opts };
  }
}

export class UpdateLoaderState {
  static readonly type = '[Loader] Update Loader State';
}
