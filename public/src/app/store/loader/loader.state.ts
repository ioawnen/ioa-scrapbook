import { State, Action, StateContext, Selector } from '@ngxs/store';
import * as LoaderActions from './loader.actions';
import { LoaderStateModel, ILoaderProcess } from './loader.model';
import * as _ from 'lodash';

@State<LoaderStateModel>({
  name: 'loader',
  defaults: {
    processes: new Map(),
    visible: false,
    mode: 'indeterminate'
  }
})
export class LoaderState {

  @Selector()
  static processes({ processes }: LoaderStateModel): Map<string, ILoaderProcess> {
    return processes;
  }

  @Selector()
  static visible({ visible }: LoaderStateModel): boolean {
    return visible;
  }

  @Selector()
  static mode({ mode }: LoaderStateModel): string {
    return mode;
  }

  @Action(LoaderActions.UpdateLoadingStatus)
  updateLoadingStatus(
    { getState, patchState, dispatch }: StateContext<LoaderStateModel>,
    { processId, opts }: LoaderActions.UpdateLoadingStatus) {

    const processes = getState().processes;
    processes.set(processId, { ...processes.get(processId), ...opts, lastUpdated: new Date() });

    patchState({ processes: processes });

    dispatch(new LoaderActions.UpdateLoaderState());
  }

  @Action(LoaderActions.UpdateLoaderState)
  updateLoaderState({ getState, patchState }: StateContext<LoaderStateModel>) {
    const processes = getState().processes;

    const loading = Array.from(processes).some(([_key, val]) => val.loading === true);

    patchState({
      visible: loading
    });
  }
}
