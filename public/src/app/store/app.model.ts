
export interface AppStateModel {
    title: string;
    loaded: boolean;
}
