import { State, Action, Selector, StateContext } from '@ngxs/store';
import { AppStateModel } from '.';
import * as AppActions from './app.actions';
import * as Env from '../../environments/environment';

@State<AppStateModel>({
    name: 'appState',
    defaults: {
        title: 'IoaScrapbook',
        loaded: false
    }
})
export class AppState {

    //#region SELECTORS
    @Selector()
    static title(state: AppStateModel) {
        if (!Env.environment.production) {
            return `${state.title} - DEV BUILD - NOT FOR LIVE!`;
        }
        return state.title;
    }
    @Selector()
    static loaded(state: AppStateModel): boolean {
        return state.loaded;
    }
    //#endregion

    //#region ACTIONS
    @Action(AppActions.SetLoaded)
    setLoaded({ patchState }: StateContext<AppStateModel>) {
        patchState({ loaded: true });
    }
    //#endregion

    constructor() { }

}
