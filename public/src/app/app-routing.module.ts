import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './base/components/home/home.component';
import { BasicShellComponent, UserShellComponent } from './layout/components';
import { LoginContentComponent, LoginPanelComponent, LoginPageComponent } from './auth/components';
import { ScrapbookComponent } from './scrapbook/components';
import { AuthenticatedGuard } from './shared/guards/authenticated.guard';

const routes: Routes = [
  {
    /** BASIC - NO NAVBAR / SIDEBAR */
    path: 'login',
    component: BasicShellComponent,
    children: [
      { path: '', component: LoginPageComponent }
    ]
  },
  /** USER - NAVBAR / SIDEBAR */
  {
    path: '',
    component: UserShellComponent,
    canActivate: [AuthenticatedGuard],
    children: [
      { path: '', component: HomeComponent },
      { path: 'sb/:scrapbookId', component: ScrapbookComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
