import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { SetSidenavOpen } from 'src/app/layout/store/layout.actions';
import { OpenCreateNewScrapbookDialog } from 'src/app/scrapbook/store/scrapbook.actions';

@Component({
  selector: 'ioa-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {

  constructor(private store: Store) { }

  ngOnInit() {
  }

  onOpenExistingClick() {
    this.store.dispatch(new SetSidenavOpen(true));
  }

  onCreateNewScrapbookClick() {
    this.store.dispatch(new OpenCreateNewScrapbookDialog());
  }

}
