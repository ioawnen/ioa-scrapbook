import { TestBed, inject } from '@angular/core/testing';

import { AuthRemoteService } from './auth-remote.service';

describe('AuthRemoteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthRemoteService]
    });
  });

  it('should be created', inject([AuthRemoteService], (service: AuthRemoteService) => {
    expect(service).toBeTruthy();
  }));
});
