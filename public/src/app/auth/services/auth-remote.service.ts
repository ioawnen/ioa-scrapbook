import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { UserInfo, AppUser } from '../store/auth-state.model';
import { User } from 'firebase';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthRemoteService {

  constructor(private afAuth: AngularFireAuth, private afStore: AngularFirestore) { }

  /**
   * Gets the full user data for an input firebase user.
   *
   * @param {User} firebaseUser Input user. This is usually the current user.
   * @returns {Observable<UserInfo>}
   * @memberof AuthRemoteService
   */
  getUserInfo(firebaseUser: User): Observable<UserInfo> {
    return this.afStore.collection<AppUser>('users')
      .doc<AppUser>(firebaseUser.uid)
      .snapshotChanges()
      .pipe(
        map(appUser => (<UserInfo>{
          firebaseUser: firebaseUser,
          appUser: appUser.payload.data()
        }))
      );


  }
}
