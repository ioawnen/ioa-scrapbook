import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginPanelComponent, LoginContentComponent } from './components';
import { MaterialModule } from '../material/material.module';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { NgxsModule, Store } from '@ngxs/store';
import { AuthState } from './store/auth.state';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { RegisterContentComponent } from './components/register-content/register-content.component';
import { InitAuthState } from './store/auth.actions';
import { switchMap, skipWhile } from 'rxjs/operators';
import { AuthRemoteService } from './services/auth-remote.service';
import * as authActions from './store/auth.actions';
import { of } from 'rxjs';
import { ChangePasswordContentComponent } from './components/change-password-content/change-password-content.component';

@NgModule({
  imports: [
    NgxsModule.forFeature([
      AuthState
    ]),
    AngularFireAuthModule,
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    LoginPanelComponent,
    LoginContentComponent,
    LoginPageComponent,
    RegisterContentComponent,
    ChangePasswordContentComponent
  ],
  providers: [
    AuthRemoteService
  ],
  exports: [
    LoginPanelComponent,
    LoginContentComponent,
    LoginPageComponent
  ],
  entryComponents: [ChangePasswordContentComponent]
})
export class AuthModule {
  constructor(store: Store, authRemoteService: AuthRemoteService) {
    store.dispatch(new InitAuthState());

    store.select(AuthState.isLoggedIn)
      .pipe(
        skipWhile((isloggedIn => !isloggedIn)),
        switchMap(() => store.select(AuthState.firebaseUser)),
        switchMap((fbU) => fbU ? authRemoteService.getUserInfo(fbU) : of(null))
      ).subscribe((userInfo) => {
        console.debug('DEBUG - Detected app user info change. Updating auth state');
        store.dispatch(new authActions.UpdateUser(userInfo));
      });
  }
}
