import { Component, OnInit, Input } from '@angular/core';
import * as AuthActions from '../../store/auth.actions';
import { Store } from '@ngxs/store';

@Component({
  selector: 'ioa-login-panel',
  templateUrl: './login-panel.component.html',
  styles: []
})
export class LoginPanelComponent implements OnInit {

  @Input() cardTitle = 'Sign in';

  public contentType: 'login' | 'register' = 'login';

  constructor(private store: Store) { }


  onRegisterToggle() {
    this.contentType = this.contentType === 'login' ? 'register' : 'login';
  }

  onSignInWithGoogleClick() {
    this.store.dispatch(new AuthActions.SignInWithGoogle());
  }

  ngOnInit() {
  }

}
