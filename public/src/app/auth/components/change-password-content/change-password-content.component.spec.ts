import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangePasswordContentComponent } from './change-password-content.component';

describe('ChangePasswordContentComponent', () => {
  let component: ChangePasswordContentComponent;
  let fixture: ComponentFixture<ChangePasswordContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangePasswordContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangePasswordContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
