import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'ioa-change-password-content',
  templateUrl: './change-password-content.component.html',
  styles: []
})
export class ChangePasswordContentComponent implements OnInit {

  public changePasswordForm: FormGroup;

  constructor(public dialogRef: MatDialogRef<ChangePasswordContentComponent>) { }

  ngOnInit() {
    this.changePasswordForm = new FormGroup({
      oldPassword: new FormControl(null, [Validators.required]),
      newPassword: new FormControl(null, [Validators.required, Validators.minLength(8)]),
      newConfirmPassword: new FormControl(null, [Validators.required])
    });
  }

  onChangePasswordFormSubmit() {
    throw new Error('Not implemented!');
  }

  onCancelClick() {
    this.dialogRef.close();
  }


}
