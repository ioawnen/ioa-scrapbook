import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import * as AuthActions from '../../store/auth.actions';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthState } from '../../store/auth.state';

@Component({
  selector: 'ioa-register-content',
  templateUrl: './register-content.component.html',
  styles: []
})
export class RegisterContentComponent implements OnInit {

  @Output() toggle = new EventEmitter<any>();
  @Select(AuthState.pending) pending$: boolean;

  public registerForm: FormGroup;

  constructor(private store: Store) {
    this.registerForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required])
    });
  }

  ngOnInit() {
  }

  onRegisterSubmit() {
    this.store.dispatch(new AuthActions.RegisterUser(this.registerForm.value.email, this.registerForm.value.password));
  }

}
