import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Store, Select } from '@ngxs/store';
import * as AuthActions from '../../store/auth.actions';
import { AuthState } from '../../store/auth.state';

@Component({
  selector: 'ioa-login-content',
  templateUrl: './login-content.component.html',
  styles: []
})
export class LoginContentComponent implements OnInit {


  @Output() toggle = new EventEmitter<any>();

  @Select(AuthState.isLoggedIn) isLoggedIn$: boolean;
  @Select(AuthState.pending) pending$: boolean;
  @Select(AuthState.rememberMe) rememberMe$: boolean;

  public emailPasswordForm: FormGroup;

  constructor(private store: Store) { }

  ngOnInit() {
    this.emailPasswordForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required]),
      rememberMe: new FormControl(this.store.selectSnapshot(AuthState.rememberMe))
    });

    this.store.select(AuthState.rememberMe).subscribe((next) =>
      this.emailPasswordForm.patchValue({ rememberMe: next }
      ));
    this.emailPasswordForm.controls.rememberMe.valueChanges.subscribe((next) => this.onRememberMeClick(next));
  }

  onEmailPasswordFormSubmit() {
    this.store.dispatch(
      new AuthActions.SignInWithEmailPassword(
        this.emailPasswordForm.value.email,
        this.emailPasswordForm.value.password
      ));
  }

  onRememberMeClick(x) {
    this.store.dispatch(new AuthActions.SetRememberMe(x));
  }

}
