import { User } from 'firebase';
import { UserInfo } from './auth-state.model';

export class SignInWithEmailPassword {
    static readonly type = '[Auth] Sign in with Email / Password ';
    constructor(public email: string, public password: string, public redirectToHome = true) { }
}
export class SignInWithGoogle {
    static readonly type = '[Auth] Sign in with Google';
    constructor(public redirectToHome = true) { }
}
export class SignInWithGitHub {
    static readonly type = '[Auth] Sign in with GitHub';
    constructor(public redirectToHome = true) { }
}
export class SignOut {
    static readonly type = '[Auth] Sign out (All)';
    constructor(public redirectToLogin = true) { }
}
export class SignInSuccess {
    static readonly type = '[Auth] Sign in SUCCESS';
    constructor(public user: User, public redirectToHome: boolean) { }
}
export class SignInFail {
    static readonly type = '[Auth] Sign in FAIL';
    constructor() { }
}

export class SignOutSuccess {
    static readonly type = '[Auth] Sign out SUCCESS';
    constructor(public redirectToLogin: boolean) { }
}
export class SignOutFail {
    static readonly type = '[Auth] Sign out FAIL';
    constructor() { }
}

export class RegisterUser {
    static readonly type = '[Auth] Register User';
    constructor(public email: string, public password: string) { }
}

export class RegisterSuccess {
    static readonly type = '[Auth] Register SUCCESS';
    constructor() { }
}
export class RegisterFail {
    static readonly type = '[Auth] Register FAIL';
    constructor() { }
}

export class SetRememberMe {
    static readonly type = '[Auth] Set Remember Me';
    constructor(public rememberMe?: boolean) { }
}

export class InitAuthState {
    static readonly type = '[Auth] Init Auth State';
    constructor() { }
}

export class InitAuthStateComplete {
    static readonly type = '[Auth] Init Auth State Complete';
    constructor() { }
}

export class UpdateUser {
    static readonly type = '[Auth] Update User';
    constructor(public user: UserInfo) { }
}
export class OpenChangePasswordDialog {
    static readonly type = '[Auth] Open Change Password Dialog';
}
