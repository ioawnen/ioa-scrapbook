import { State, Action, Selector, StateContext } from '@ngxs/store';
import { AuthStateModel, UserInfo, AppUser } from './auth-state.model';
import { AngularFireAuth } from '@angular/fire/auth';
import { User, auth } from 'firebase';

import * as AuthActions from './auth.actions';
import { Navigate } from '@ngxs/router-plugin';

import * as _ from 'lodash';
import { timeout, catchError, take, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { AuthRemoteService } from '../services/auth-remote.service';
import { MatDialog } from '@angular/material';
import { ChangePasswordContentComponent } from '../components/change-password-content/change-password-content.component';

@State<AuthStateModel>({
    name: 'authState',
    defaults: {
        isLoggedIn: false,
        pending: false,
        user: null,
        rememberMe: false,
        authStateInitialised: false
    }
})
export class AuthState {

    //#region SELECTORS
    @Selector()
    static isLoggedIn({ isLoggedIn }: AuthStateModel): boolean {
        return isLoggedIn;
    }
    @Selector()
    static pending({ pending }: AuthStateModel): boolean {
        return pending;
    }

    @Selector()
    static user({ user }: AuthStateModel): UserInfo {
        return user;
    }

    @Selector()
    static firebaseUser({ user }: AuthStateModel): User {
        return user.firebaseUser;
    }

    @Selector()
    static appUser({ user }: AuthStateModel): AppUser {
        return user.appUser;
    }


    @Selector()
    static rememberMe({ rememberMe }: AuthStateModel): boolean {
        return rememberMe;
    }

    @Selector()
    static authStateInitialised({ authStateInitialised }: AuthStateModel): boolean {
        return authStateInitialised;
    }
    //#endregion

    //#region ACTIONS
    @Action(AuthActions.SignInSuccess)
    signInSuccess({ patchState, dispatch }: StateContext<AuthStateModel>, { user, redirectToHome }: AuthActions.SignInSuccess) {
        this.authRemoteService.getUserInfo(user).pipe(take(1)).subscribe(userInfo => {
            patchState({
                user: userInfo,
                isLoggedIn: true,
                pending: false
            });
            if (redirectToHome) {
                dispatch(new Navigate(['/']));
            }
        })
    }

    @Action(AuthActions.SignInFail)
    signInFail({ patchState }: StateContext<AuthStateModel>, { }: AuthActions.SignInFail) {
        patchState({
            isLoggedIn: false,
            pending: false
        });
    }

    @Action(AuthActions.SignInWithEmailPassword)
    signInWithEmailPassword(
        { dispatch, patchState }: StateContext<AuthStateModel>,
        { email, password, redirectToHome }: AuthActions.SignInWithEmailPassword
    ) {
        patchState({ pending: true });
        this.afAuth.auth.signInWithEmailAndPassword(email, password).then(
            (next) => dispatch(new AuthActions.SignInSuccess(next.user, redirectToHome)),
            () => dispatch(new AuthActions.SignInFail())
        );
    }

    @Action(AuthActions.SignInWithGoogle)
    signInWithGoogle({ dispatch, patchState }: StateContext<AuthStateModel>, { redirectToHome }: AuthActions.SignInWithGoogle) {
        patchState({ pending: true });
        this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider()).then(
            (next) => dispatch(new AuthActions.SignInSuccess(next.user, redirectToHome)),
            () => dispatch(new AuthActions.SignInFail())
        );
    }

    @Action(AuthActions.SignOutSuccess)
    signOutSuccess({ patchState, dispatch }: StateContext<AuthStateModel>, { redirectToLogin }: AuthActions.SignOutSuccess) {
        patchState({
            user: null,
            isLoggedIn: false,
            pending: false
        });
        if (redirectToLogin) {
            dispatch(new Navigate(['/login'], { 'r': 'signout' }));
        }
    }

    @Action(AuthActions.SignOutFail)
    signOutFail({ patchState }: StateContext<AuthStateModel>, { }: AuthActions.SignOutFail) {
        patchState({ pending: false, isLoggedIn: true });
    }

    @Action(AuthActions.SignOut)
    signOut({ dispatch, patchState }: StateContext<AuthStateModel>, { redirectToLogin }: AuthActions.SignOut) {
        patchState({ pending: true, isLoggedIn: false });
        this.afAuth.auth.signOut().then(
            () => dispatch(new AuthActions.SignOutSuccess(redirectToLogin)),
            () => dispatch(new AuthActions.SignOutFail())
        );
    }

    @Action(AuthActions.RegisterUser)
    registerUser({ dispatch, patchState }: StateContext<AuthStateModel>, { email, password }: AuthActions.RegisterUser) {
        patchState({ pending: true });
        this.afAuth.auth.createUserWithEmailAndPassword(email, password).then(
            () => {
                dispatch(new AuthActions.RegisterSuccess());
                dispatch(new AuthActions.SignInWithEmailPassword(email, password));
            },
            () => dispatch(new AuthActions.RegisterFail()));
    }

    @Action(AuthActions.RegisterSuccess)
    registerSuccess({ patchState }: StateContext<AuthStateModel>, { }: AuthActions.RegisterSuccess) {
        patchState({ pending: false });
    }

    @Action(AuthActions.RegisterFail)
    registerFail({ patchState }: StateContext<AuthStateModel>, { }: AuthActions.RegisterFail) {
        patchState({ pending: false });
    }

    @Action(AuthActions.SetRememberMe)
    setRememberMe({ patchState, getState }: StateContext<AuthStateModel>, { rememberMe }: AuthActions.SetRememberMe) {
        const rem = _.isNil(rememberMe) ? !getState().rememberMe : rememberMe;
        this.afAuth.auth.setPersistence(rem ? 'local' : 'none').then(() =>
            patchState({ rememberMe: rem })
        );
    }

    @Action(AuthActions.InitAuthState)
    initAuthState({ patchState, dispatch }: StateContext<AuthStateModel>, { }: AuthActions.InitAuthState) {
        this.afAuth.authState.pipe(
            take(1),
            timeout(2000), // Just in case this takes too long, assume not persisting and continue
            take(1),
            catchError(err => {
                console.warn('Timed out fetching persisted user.', err);
                return of(null);
            }),
            take(1),
            switchMap(firebaseUser => firebaseUser ? this.authRemoteService.getUserInfo(firebaseUser) : of(null)),
            take(1)
        ).subscribe((user: UserInfo) => {
            patchState({ user: user, isLoggedIn: user ? true : false, rememberMe: user ? true : false });
            dispatch(new AuthActions.InitAuthStateComplete());
        }, () => dispatch(new AuthActions.InitAuthStateComplete()));
    }

    @Action(AuthActions.InitAuthStateComplete)
    initAuthStateComplete({ patchState }: StateContext<AuthStateModel>) {
        patchState({ authStateInitialised: true });
    }

    @Action(AuthActions.UpdateUser)
    updateUser({ patchState }: StateContext<AuthStateModel>, { user }: AuthActions.UpdateUser) {
        patchState({ user: user });
    }

    @Action(AuthActions.OpenChangePasswordDialog)
    openChangePasswordDialog({ }: StateContext<AuthStateModel>, { }: AuthActions.OpenChangePasswordDialog) {
        const ref = this.matDialog.open(ChangePasswordContentComponent);
        ref.beforeClose().subscribe(() => ref.close());
    }

    //#endregion

    constructor(private afAuth: AngularFireAuth, private authRemoteService: AuthRemoteService, private matDialog: MatDialog) {
    }

}
