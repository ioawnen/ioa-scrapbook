import { User } from 'firebase';

/**
 * Supported user roles in the application
 *
 * @export
 * @enum {number}
 */
export enum UserRoles { 'none', 'user', 'admin' }

/**
 * Application specific user data. This holds things the default Firebase implemention does not
 *
 * @export
 * @interface AppUser
 */
export interface AppUser {

    active: boolean;
    role: UserRoles;
    firstTime: boolean;
}

/**
 * Contains Firebase and custom application specific user data
 *
 * @export
 * @interface UserInfo
 */
export interface UserInfo {
    /** Instance of firebase user data  */
    firebaseUser: User;
    /** Instance of application user data */
    appUser: AppUser;
}

export interface AuthStateModel {
    isLoggedIn: boolean;
    pending: boolean;
    user: UserInfo;
    rememberMe: boolean;
    authStateInitialised: boolean;
}
