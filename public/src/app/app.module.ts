import { BrowserModule } from '@angular/platform-browser';
import { NgModule, PLATFORM_ID, APP_ID, Inject, ErrorHandler, Injectable } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { NgxsModule, Store, Actions } from '@ngxs/store';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './material/material.module';
import { AppState } from './store';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from './layout/layout.module';
import { BaseModule } from './base/base.module';
import { AuthModule } from './auth/auth.module';
import { ScrapbookModule } from './scrapbook/scrapbook.module';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';
import { LoaderState } from './store/loader/loader.state';
import * as LoaderActions from './store/loader/loader.actions';

@Injectable()
export class IoaErrorHandler implements ErrorHandler {
  constructor(private toastr: ToastrService) { }
  handleError(error: any): void {
    console.error(error);
    this.toastr.error(_.get(error, ['message'], 'An unknown error occured. Please try again.'), 'Error');
  }
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ioa-scrapbook-web-ssr' }),
    AngularFireModule.initializeApp(environment.firebase, 'ioa-scrapbook-web'),
    AngularFirestoreModule,
    AngularFireAuthModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgxsModule.forRoot([
      AppState,
      LoaderState
    ]),
    NgxsRouterPluginModule.forRoot(),
    NgxsLoggerPluginModule.forRoot(),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    // NgxsStoragePluginModule.forRoot({}),
    AppRoutingModule,
    MaterialModule,
    LayoutModule,
    AuthModule,
    BaseModule,
    ScrapbookModule
  ],
  providers: [
    IoaErrorHandler,
    {
      provide: ErrorHandler,
      useClass: IoaErrorHandler
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string
  ) {
    const platform = isPlatformBrowser(platformId) ?
      'in the browser' : 'on the server';
    console.log(`Running ${platform} with appId=${appId}`);

  }
}
