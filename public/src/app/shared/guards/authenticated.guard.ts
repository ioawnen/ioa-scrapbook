import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Store } from '@ngxs/store';
import { AuthState } from '../../auth/store/auth.state';
import { Navigate } from '@ngxs/router-plugin';
import { switchMap, take, skipWhile } from 'rxjs/operators';

/**
 * @description Prevents access to routes if the user hasn't authenticated. The user is redirected to login if not.
 * @author ioawnen
 * @date 2018-09-11
 * @export
 * @class AuthenticatedGuard
 * @implements {CanActivate}
 */
@Injectable({
  providedIn: 'root'
})
export class AuthenticatedGuard implements CanActivate {

  constructor(private store: Store) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    return (
      this.store.selectSnapshot(AuthState.authStateInitialised) ? // Has the auth state finished initialising?
        of(true) :  // Yes, emit immediately
        this.store.select(AuthState.authStateInitialised).pipe(skipWhile(x => !x)) // No, wait for it then emit
    ).pipe(
      take(1),
      switchMap((x) => {
        if (this.store.selectSnapshot(AuthState.isLoggedIn)) {  // Is the user logged in?
          return of(true);
        } else {  // No, redirect to login
          this.store.dispatch(new Navigate(['/login'], { 'r': 'auth_g' }));
          return of(false);
        }
      })
    );
  }
}
