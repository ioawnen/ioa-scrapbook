import { Component } from '@angular/core';
import { speedDialFabAnimations } from './speed-dial-fab-animations';


@Component({
  selector: 'ioa-speed-dial-fab',
  templateUrl: './speed-dial-fab.component.html',
  styleUrls: ['./speed-dial-fab.component.scss'],
  animations: speedDialFabAnimations
})
export class SpeedDialFabComponent {

  fabTogglerState = 'inactive';

  constructor() { }

  showItems() {
    this.fabTogglerState = 'active';
  }

  hideItems() {
    this.fabTogglerState = 'inactive';
  }

  onToggleFab() {
    this.fabTogglerState === 'active' ? this.hideItems() : this.showItems();
  }
}
