import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicShellComponent } from './basic-shell.component';

describe('BasicShellComponent', () => {
  let component: BasicShellComponent;
  let fixture: ComponentFixture<BasicShellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BasicShellComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
