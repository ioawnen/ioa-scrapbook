import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { LayoutState } from '../../store/layout.state';
import { Observable } from 'rxjs';
import { ScrapbookState } from '../../../scrapbook/store';
import { IScrapbookBundle } from '../../../scrapbook/models/scrapbook.model';
import * as ScrapbookActions from '../../../scrapbook/store/scrapbook.actions';
import { MatDialog } from '@angular/material';
import * as LayoutActions from '../../store/layout.actions';
import { AuthState } from '../../../auth/store/auth.state';
import * as AuthActions from '../../../auth/store/auth.actions';
import { User } from 'firebase';
import { Navigate } from '@ngxs/router-plugin';
import { LoaderState } from 'src/app/store/loader/loader.state';

@Component({
  selector: 'ioa-sidenav',
  templateUrl: './sidenav.component.html',
  styles: []
})
export class SidenavComponent implements OnInit {
  @Select(LayoutState.sidenavOpen) sidenavOpen$: Observable<boolean>;
  @Select(AuthState.user) user$: Observable<User>;
  @Select(ScrapbookState.scrapbooks) scrapbooks$: Observable<IScrapbookBundle[]>;
  @Select(ScrapbookState.currentScrapbookId) currentSbId$: Observable<string>;
  @Select(LoaderState.visible) public visible$: Observable<boolean>;
  @Select(LoaderState.mode) public mode$: Observable<string>;

  constructor(private store: Store, public dialog: MatDialog) { }

  ngOnInit() { }

  toggleSidenavOpen() {
    this.store.dispatch(new LayoutActions.ToggleSidenavOpen());
  }

  onTopLogoClick() {
    this.store.dispatch(new Navigate(['/']));
  }

  onNewScrapbookClick() {
    this.store.dispatch(new ScrapbookActions.OpenCreateNewScrapbookDialog);
  }

  onScrapbookLinkClick(sb: IScrapbookBundle) {
    this.store.dispatch(new Navigate(['sb', sb.scrapbookId]));
  }

  onSignOutClick() {
    this.store.dispatch(new AuthActions.SignOut);
  }

  onManageAccountClick() {
    this.store.dispatch(new Navigate(['user/settings']));
  }

  onChangePasswordClick() {
    this.store.dispatch(new AuthActions.OpenChangePasswordDialog);
  }

}
