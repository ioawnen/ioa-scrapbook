import { Component, OnInit } from '@angular/core';
import { Store, Select, Actions } from '@ngxs/store';
import * as LayoutActions from '../../store/layout.actions';
import { AppState } from '../../../store';
import { LayoutState } from '../../store/layout.state';
import { Observable } from 'rxjs';
import * as AuthActions from '../../../auth/store/auth.actions';
import { AuthState } from '../../../auth/store/auth.state';
import { User } from 'firebase';

/**
 *
 * @deprecated
 * @export
 * @class ToolbarComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'ioa-toolbar',
  templateUrl: './toolbar.component.html',
  styles: []
})
export class ToolbarComponent implements OnInit {

  @Select(AppState.title) title$: Observable<string>;
  @Select(LayoutState.sidenavOpen) sidenavOpen$: Observable<boolean>;
  @Select(AuthState.user) user$: Observable<User>;

  constructor(private store: Store) { }
  ngOnInit() {
  }

  toggleSidenavOpen() {
    this.store.dispatch(new LayoutActions.ToggleSidenavOpen());
  }

  onSignOutClick() {
    this.store.dispatch(new AuthActions.SignOut);
  }

}
