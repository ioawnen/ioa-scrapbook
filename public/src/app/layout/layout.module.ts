import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material/material.module';
import { NgxsModule } from '@ngxs/store';
import { LayoutState } from './store/layout.state';
import { AppRoutingModule } from '../app-routing.module';
import { UserShellComponent, BasicShellComponent } from './components';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { SpeedDialFabComponent } from './components/speed-dial-fab/speed-dial-fab.component';

@NgModule({
  imports: [
    NgxsModule.forFeature([
      LayoutState
    ]),
    CommonModule,
    AppRoutingModule,
    MaterialModule
  ],
  declarations: [UserShellComponent, BasicShellComponent, SidenavComponent, ToolbarComponent, SpeedDialFabComponent],
  exports: [UserShellComponent, BasicShellComponent, SpeedDialFabComponent]
})
export class LayoutModule { }
