import { State, Action, Selector, StateContext } from '@ngxs/store';
import { LayoutStateModel } from './layout.model';
import * as LayoutActions from './layout.actions';


@State<LayoutStateModel>({
    name: 'layoutState',
    defaults: {
        sidenavOpen: false
    }
})
export class LayoutState {

    @Selector()
    static sidenavOpen(state: LayoutStateModel) {
        return state.sidenavOpen;
    }


    @Action(LayoutActions.ToggleSidenavOpen)
    toggleSidenavOpen({ patchState, getState }: StateContext<LayoutStateModel>) {
        patchState({ sidenavOpen: !getState().sidenavOpen });
    }


    @Action(LayoutActions.SetSidenavOpen)
    setSidenavOpen({ patchState }: StateContext<LayoutStateModel>, { open }: LayoutActions.SetSidenavOpen) {
        patchState({ sidenavOpen: open });
    }
}
