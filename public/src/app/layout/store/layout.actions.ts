
export class ToggleSidenavOpen {
    static type = '[Layout] Toggle Sidenav Open';
}

export class SetSidenavOpen {
    static type = '[Layout] Set Sidenav Open';
    constructor(public open: boolean) { }
}
