import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScrapbookContentComponent } from './scrapbook-content.component';

describe('ScrapbookContentComponent', () => {
  let component: ScrapbookContentComponent;
  let fixture: ComponentFixture<ScrapbookContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScrapbookContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScrapbookContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
