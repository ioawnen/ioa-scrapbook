import { Component, OnInit, ComponentFactoryResolver, ViewChildren, ElementRef, Renderer2 } from '@angular/core';
import { ElementFactory } from '../elements/element-factory/element-factory';
import { TextboxComponent } from '../elements/textbox/textbox.component';
import { Store, Select } from '@ngxs/store';
import { ScrapbookState } from '../../store';
import { Observable, of } from 'rxjs';
import { IScrapbook, IScrapbookBundle } from '../../models/scrapbook.model';
import * as _ from 'lodash';
import { IElement, IElementBundle } from '../../models/element.model';
import { map, switchMap, distinctUntilChanged, concatMap } from 'rxjs/operators';
import * as interact from 'interactjs';

@Component({
    selector: 'ioa-scrapbook-content',
    templateUrl: './scrapbook-content.component.html',
    styles: []
})
export class ScrapbookContentComponent implements OnInit {
    @ViewChildren('elFactory') elFactories: ElementRef[];
    @Select(ScrapbookState.currentScrapbook) currentScrapbook$: Observable<IScrapbookBundle>;
    @Select(ScrapbookState.currentScrapbookElements) currentElements$: Observable<IElementBundle[]>;
    @Select(ScrapbookState.currentScrapbookElementById) currentSbById$: Observable<(id: string) => IElementBundle>;

    public elements$: Observable<IElementBundle[]>;

    getElementById(id: string) {
        return this.currentSbById$.pipe(map(x => x(id)));
    }

    constructor() { }

    ngOnInit() {
        this.elements$ = of([]).pipe(
            concatMap(() => this.currentElements$),
            distinctUntilChanged()
        );
    }


    trackElement(index: number, element: IElementBundle) {
        return element.elementId;
    }


}
