import { Component, OnInit, ElementRef } from '@angular/core';
import { ElementBase } from '../element-base';
import { Store } from '@ngxs/store';

@Component({
  selector: 'ioa-shape',
  templateUrl: './shape.component.html',
  styles: [],
})
export class ShapeComponent extends ElementBase implements OnInit {

  constructor(public store: Store, public el: ElementRef) {
    super(store, el);
  }
  ngOnInit() {
  }

}
