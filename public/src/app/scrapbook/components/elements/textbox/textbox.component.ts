import { Component, OnInit, ElementRef, ViewChild, NgZone } from '@angular/core';
import { ElementBase } from '../element-base';
import { Store } from '@ngxs/store';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { take } from 'rxjs/operators';


@Component({
  selector: 'ioa-textbox',
  templateUrl: './textbox.component.html',
  styles: []
})
export class TextboxComponent extends ElementBase implements OnInit {
  @ViewChild('autosize') autosize: CdkTextareaAutosize;

  constructor(public store: Store, public el: ElementRef, private ngZone: NgZone) {
    super(store, el);
  }

  ngOnInit() {
  }


  triggerResize() {
    // Wait for changes to be applied, then trigger textarea resize.
    this.ngZone.onStable.pipe(take(1))
      .subscribe(() => this.autosize.resizeToFitContent(true));
  }

  onValueChanges(val) {
    this.dispatchElementDataUpdate({ data: val });
  }

}
