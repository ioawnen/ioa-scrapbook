import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
    selector: '[ioaElementHost]'
})
export class ElementHostDirective {

    constructor(public viewContainerRef: ViewContainerRef) { }

}
