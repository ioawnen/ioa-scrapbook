import { Input, ElementRef, HostBinding } from '@angular/core';
import { Observable } from 'rxjs';
import { IElement, IElementBundle } from '../../models/element.model';
import { Store } from '@ngxs/store';
import * as ScrapbookActions from '../../store/scrapbook.actions';
import { take } from 'rxjs/operators';
export class ElementBase {

    @HostBinding('class.element') _elementClass = true;

    @Input() element$: Observable<IElementBundle>;
    @Input() elementId: string;

    dispatchUpdate(element: IElementBundle) {
        this.store.dispatch(new ScrapbookActions.UpdateScrapbookElement(element));
    }
    dispatchElementDataUpdate(element: Partial<IElement>) {
        this.element$.pipe(take(1)).subscribe(el => this.dispatchUpdate({ ...el, element: { ...el.element, ...element } }));
    }

    constructor(public store: Store, public el: ElementRef) { }

}
