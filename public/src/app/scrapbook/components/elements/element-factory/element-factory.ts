import { Component, ViewContainerRef, OnInit, ViewChild, ComponentFactoryResolver, AfterViewInit, Input, ElementRef } from '@angular/core';
import { ElementHostDirective } from '../element-host.directive';
import { Select, Store } from '@ngxs/store';
import { ScrapbookState } from '../../../store/scrapbook.state';
import { Observable } from 'rxjs';
import { IScrapbook } from '../../../models/scrapbook.model';
import { TextboxComponent } from '../textbox/textbox.component';
import { IElement, IElementBundle } from '../../../models/element.model';
import { ShapeComponent } from '../shape/shape.component';
import { take } from 'rxjs/operators';
import * as interact from 'interactjs';
import * as ScrapbookActions from '../../../store/scrapbook.actions';
import { ScrapbookUXState, ModeType } from 'src/app/scrapbook/store/scrapbook-ux/scrapbook-ux.state';
import * as LoaderActions from 'src/app/store/loader/loader.actions';
import { SbGuid } from 'src/app/utils/guid';

@Component({
  selector: 'ioa-element-factory',
  template: `
  <div #elWrapper class="element-wrapper" *ngIf="(element | async) as el"
    [ngStyle]="{
      'top': el.element.position?.top + 'px',
      'left': el.element.position?.left + 'px',
      'height': el.element.dimensions?.height + 'px',
      'width': el.element.dimensions?.width + 'px',
      'z-index': el.element.position?.level
    }">
    <ng-template ioaElementHost></ng-template>
  </div>
`
})
// tslint:disable-next-line:component-class-suffix
export class ElementFactory implements AfterViewInit {
  @ViewChild(ElementHostDirective) ioaElementHost: ElementHostDirective;
  @ViewChild('elWrapper') elementWrapper: ElementRef;
  @ViewChild('resizeCorner') resizeCorner: ElementRef;

  @Input() element: Observable<IElementBundle>;

  @Select(ScrapbookUXState.mode) mode$: Observable<ModeType>;

  private instGuid: string;

  public _initialX: number;
  public _initialY: number;

  elementMap = {
    'text': TextboxComponent,
    'shape': ShapeComponent
  };

  interactEl: interact.Interactable;

  constructor(private store: Store, private componentFactoryResolver: ComponentFactoryResolver) {
    this.instGuid = SbGuid.newGuid();

    store.dispatch(new LoaderActions.UpdateLoadingStatus(this.instGuid, { loading: true }));
  }

  ngAfterViewInit() {
    setTimeout(() => this.loadElement(), 1000);

    this.interactEl = interact(this.elementWrapper.nativeElement);
    this.mode$.subscribe(m => this.updateDraggable(m === ModeType.MOVE));
  }

  updateDraggable(enabled: boolean): void {
    if (enabled) {
      this.interactEl.draggable({
        restrict: {
          restriction: '.content-area',
          endOnly: true,
          elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
        },
        autoScroll: true,
        inertia: true,
        onmove: (e) => this.dragMoveListener(e),
        onstart: (e) => this.onMoveStart(e),
        onend: (e) => this.onMoveEnd(e)
      });
    } else {
      this.interactEl.draggable({});
    }
  }

  onMoveStart(event) {
    // Update initial positions
    this._initialX = event.clientX;
    this._initialY = event.clientY;
  }

  onMoveEnd(event) {
    const deltaX = event.clientX - this._initialX;
    const deltaY = event.clientY - this._initialY;

    // Get current element
    this.element.pipe(take(1)).subscribe(el => {
      // Calculate new position, dispatch update
      el.element.position = {
        ...el.element.position,
        top: el.element.position.top + deltaY,
        left: el.element.position.left + deltaX
      };
      this.store.dispatch(new ScrapbookActions.UpdateScrapbookElement(el));

      // Remove transform
      event.target.style.tranform = event.target.style.webkitTransform = '';
      event.target.setAttribute('data-x', null);
      event.target.setAttribute('data-y', null);
    });
  }

  dragMoveListener(event) {
    if (this.store.selectSnapshot(ScrapbookUXState.mode) === ModeType.MOVE) {
      const target = event.target;
      // keep the dragged position in the data-x/data-y attributes
      const x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx;
      const y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

      // translate the element
      target.style.webkitTransform =
        target.style.transform =
        'translate(' + x + 'px, ' + y + 'px)';

      // update the position attributes
      target.setAttribute('data-x', x);
      target.setAttribute('data-y', y);
    }
  }


  loadElement() {
    this.element.pipe(take(1)).subscribe(el => {
      console.debug(`ElementFactory::LoadElement - elementId: ${el.elementId}`, el);
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.elementMap[el.element.type]);
      const viewContainerRef = this.ioaElementHost.viewContainerRef;
      viewContainerRef.clear();

      const componentRef = viewContainerRef.createComponent(componentFactory);
      (componentRef.instance as any).element$ = this.element;
      (componentRef.instance as any).elementId = el.elementId;
      this.store.dispatch(new LoaderActions.UpdateLoadingStatus(this.instGuid, { loading: false }));
    });
  }




}
