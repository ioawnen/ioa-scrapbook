import { Component, OnInit } from '@angular/core';
import { ModeType, ScrapbookUXState } from '../../store/scrapbook-ux/scrapbook-ux.state';
import { Store, Select } from '@ngxs/store';
import * as ScrapbookUXActions from '../../store/scrapbook-ux/scrapbook-ux.actions';
import { Observable } from 'rxjs';

@Component({
  selector: 'ioa-scrapbook-overlay',
  templateUrl: './scrapbook-overlay.component.html',
  styles: []
})
export class ScrapbookOverlayComponent implements OnInit {

  modes = ModeType;

  @Select(ScrapbookUXState.mode) mode$: Observable<ModeType>;


  constructor(private store: Store) { }

  ngOnInit() {
  }

  updateMode(mode: ModeType) {
    this.store.dispatch(new ScrapbookUXActions.SetMode(mode));
  }

}
