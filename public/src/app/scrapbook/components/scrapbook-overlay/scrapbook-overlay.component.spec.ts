import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScrapbookOverlayComponent } from './scrapbook-overlay.component';

describe('ScrapbookOverlayComponent', () => {
  let component: ScrapbookOverlayComponent;
  let fixture: ComponentFixture<ScrapbookOverlayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScrapbookOverlayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScrapbookOverlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
