import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewScrapbookContentComponent } from './new-scrapbook-content.component';

describe('NewScrapbookContentComponent', () => {
  let component: NewScrapbookContentComponent;
  let fixture: ComponentFixture<NewScrapbookContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewScrapbookContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewScrapbookContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
