import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { Store } from '@ngxs/store';
import * as ScrapbookActions from '../../store/scrapbook.actions';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ScrapbookBuilder } from '../../models/scrapbook.builder';
import { AuthState } from '../../../auth/store/auth.state';

@Component({
  selector: 'ioa-new-scrapbook-content',
  templateUrl: './new-scrapbook-content.component.html',
  styles: []
})
export class NewScrapbookContentComponent implements OnInit {

  scrapbookForm: FormGroup;
  scrapbook = new ScrapbookBuilder();

  constructor(public dialogRef: MatDialogRef<NewScrapbookContentComponent>,
    public store: Store) {

    this.scrapbookForm = new FormGroup({
      title: new FormControl(this.scrapbook.title, [Validators.required]),
      iconBgColor: new FormControl(this.scrapbook.iconBgColor, [Validators.required])
    });
    console.warn(this.scrapbook.iconBgColor);
  }

  ngOnInit() {
  }

  onCreateSubmit() {
    this.store.dispatch(new ScrapbookActions.CreateNewScrapbook(
      this.scrapbook
        .setCreatorId(this.store.selectSnapshot(AuthState.firebaseUser).uid)
        .setTitle(this.scrapbookForm.value.title)
        .setIconBgColor(this.scrapbookForm.value.iconBgColor).build()
    ));
    this.dialogRef.close();
    return false;
  }

  onCancelClick() {
    this.dialogRef.close();
    return false;
  }

}
