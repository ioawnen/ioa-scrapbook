import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScrapbookSettingsContentComponent } from './scrapbook-settings-content.component';

describe('ScrapbookSettingsContentComponent', () => {
  let component: ScrapbookSettingsContentComponent;
  let fixture: ComponentFixture<ScrapbookSettingsContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScrapbookSettingsContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScrapbookSettingsContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
