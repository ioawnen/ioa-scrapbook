import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { ScrapbookRemoteService } from '../../services/scrapbook-remote.service';
import * as ScrapbookActions from '../../store/scrapbook.actions';
import { ActivatedRoute } from '@angular/router';
import { switchMap, take } from 'rxjs/operators';

@Component({
  selector: 'ioa-scrapbook',
  templateUrl: './scrapbook.component.html',
  styles: []
})
export class ScrapbookComponent implements OnInit {

  constructor(
    private store: Store,
    private sbRemoteService: ScrapbookRemoteService,
    private aR: ActivatedRoute
  ) { }

  ngOnInit() {
    // console.log('INIT');
    // this.sbRemoteService.getScrapbook(this.aR.snapshot.paramMap.get('scrapbookId')).subscribe(sb =>
    //   this.store.dispatch(new ScrapbookActions.UpdateCurrentScrapbook(sb)));

    this.aR.paramMap.pipe(
      switchMap(pM => this.sbRemoteService.getScrapbook(pM.get('scrapbookId')).pipe(take(1))),
    ).subscribe(sb => this.store.dispatch(new ScrapbookActions.UpdateCurrentScrapbook(sb)));
  }

}
