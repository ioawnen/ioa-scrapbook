import { State, Action, StateContext, Selector } from '@ngxs/store';
import * as ScrapbookUXActions from './scrapbook-ux.actions';

export enum ModeType {
  SELECT,
  MOVE,
  SHAPE,
  TEXTBOX
}

export class ScrapbookUXStateModel {
  public mode: ModeType;
}

@State<ScrapbookUXStateModel>({
  name: 'scrapbookUXState',
  defaults: {
    mode: ModeType.SELECT
  }
})
export class ScrapbookUXState {

  @Selector()
  static mode({ mode }: ScrapbookUXStateModel): ModeType {
    return mode;
  }

  @Action(ScrapbookUXActions.SetMode)
  setMode({ patchState }: StateContext<ScrapbookUXStateModel>, { mode }: ScrapbookUXActions.SetMode) {
    patchState({ mode: mode });
  }
}
