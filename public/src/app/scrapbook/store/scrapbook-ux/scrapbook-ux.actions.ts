import { ModeType } from './scrapbook-ux.state';

export class ScrapbookUXAction {
  static readonly type = '[ScrapbookUX] Add item';
  constructor(public payload: string) { }
}

export class SetMode {
  static readonly type = '[ScrapbookUX] Set Mode';
  constructor(public mode: ModeType) { }
}
