import { TestBed, async } from '@angular/core/testing';
import { NgxsModule, Store } from '@ngxs/store';
import { ScrapbookUXState } from './scrapbook-ux.state';
import { ScrapbookUXAction } from './scrapbook-ux.actions';

describe('ScrapbookUx actions', () => {
  let store: Store;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([ScrapbookUXState])]
    }).compileComponents();
    store = TestBed.get(Store);
  }));

  it('should create an action and add an item', () => {
    store.dispatch(new ScrapbookUXAction('item-1'));
    store.select(state => state.scrapbookUX.items).subscribe((items: string[]) => {
      expect(items).toEqual(jasmine.objectContaining(['item-1']));
    });
  });

});
