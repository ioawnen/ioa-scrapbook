import { IScrapbook, IScrapbookBundle } from '../models/scrapbook.model';
import { IElement, IElementBundle } from '../models/element.model';

export class PrepareWorkArea {
    static readonly type = '[Scrapbook] Prepare work area';
    constructor(public boardId: string) { }
}

export class UpdateScrapbooks {
    static readonly type = '[Scrapbook] Update scrapbooks';
    constructor(public scrapbooks: IScrapbookBundle[]) { }
}

export class UpdateCurrentScrapbook {
    static readonly type = '[Scrapbook] Update current scrapbooks';
    constructor(public scrapbook: IScrapbookBundle) { }
}

export class CreateNewScrapbook {
    static readonly type = '[Scrapbook] Create new scrapbook';
    constructor(public scrapbook?: IScrapbook) { }
}

export class OpenCreateNewScrapbookDialog {
    static readonly type = '[Scrapbook] Open create new scrapbook dialog';
}

export class UpdateScrapbookElement {
    static readonly type = '[Scrapbook] Update scrapbook element (by bundle)';
    constructor(public element: PartialExcept<IElementBundle, 'elementId'>) { }
}

export class UpdateCurrentScrapbookElement {
    static readonly type = '[Scrapbook] Update element of current scrapbook';
    constructor(public elId: string, public element: Partial<IElementBundle>) { }
}
