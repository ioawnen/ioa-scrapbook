import { State, Action, Selector, StateContext, Select } from '@ngxs/store';
import { ScrapbookStateModel } from './scrapbook.model';

import * as ScrapbookActions from './scrapbook.actions';
import { ScrapbookRemoteService } from '../services/scrapbook-remote.service';
import { IScrapbook, IScrapbookBundle } from '../models/scrapbook.model';
import { MatDialog } from '@angular/material';
import { NewScrapbookContentComponent } from '../components/new-scrapbook-content/new-scrapbook-content.component';
import { IElement, IElementBundle } from '../models/element.model';

import * as _ from 'lodash';
import { ScrapbookUXState } from './scrapbook-ux/scrapbook-ux.state';

@State<ScrapbookStateModel>({
    name: 'scrapbookState',
    defaults: {
        scrapbooks: [],
        currentScrapbook: null
    },
    children: [ScrapbookUXState]
})
export class ScrapbookState {

    //#region SELECTORS
    @Selector()
    static scrapbooks({ scrapbooks }: ScrapbookStateModel): IScrapbookBundle[] {
        return scrapbooks;
    }

    @Selector()
    static currentScrapbook({ currentScrapbook }: ScrapbookStateModel): IScrapbookBundle {
        return currentScrapbook;
    }

    @Selector()
    static currentScrapbookId({ currentScrapbook }: ScrapbookStateModel): string {
        return currentScrapbook.scrapbookId;
    }

    @Selector()
    static currentScrapbookElements({ currentScrapbook }: ScrapbookStateModel): IElementBundle[] {
        return currentScrapbook.elements;
    }

    @Selector()
    static currentScrapbookElementById({ currentScrapbook }: ScrapbookStateModel): (elementId: string) => IElementBundle {
        return (elementId: string) => currentScrapbook.elements.find(el => el.elementId === elementId);
    }

    //#endregion

    //#region ACTIONS
    @Action(ScrapbookActions.PrepareWorkArea)
    prepareWorkArea({ }: StateContext<ScrapbookStateModel>, { boardId }: ScrapbookActions.PrepareWorkArea) {

    }

    @Action(ScrapbookActions.UpdateScrapbooks)
    updateScrapbooks({ patchState }: StateContext<ScrapbookStateModel>, { scrapbooks }: ScrapbookActions.UpdateScrapbooks) {
        patchState({
            scrapbooks: scrapbooks
        });
    }

    @Action(ScrapbookActions.UpdateCurrentScrapbook)
    updateCurrentScrapbook({ patchState }: StateContext<ScrapbookStateModel>, { scrapbook }: ScrapbookActions.UpdateCurrentScrapbook) {
        patchState({
            currentScrapbook: scrapbook
        });
    }

    @Action(ScrapbookActions.CreateNewScrapbook)
    createNewScrapbook({ patchState }: StateContext<ScrapbookStateModel>, { scrapbook }: ScrapbookActions.CreateNewScrapbook) {
        this.scrapbookRemoteService.createScrapbook(scrapbook);
    }

    @Action(ScrapbookActions.OpenCreateNewScrapbookDialog)
    openCreateNewScrapbookDialog({ }: StateContext<ScrapbookStateModel>, { }: ScrapbookActions.OpenCreateNewScrapbookDialog) {
        this.matDialog.open(NewScrapbookContentComponent, {});
    }

    @Action(ScrapbookActions.UpdateScrapbookElement)
    updateScrapbookElement(
        { patchState, getState }: StateContext<ScrapbookStateModel>,
        { element }: ScrapbookActions.UpdateScrapbookElement) {
        const currentSb = getState().currentScrapbook;
        if (_.get(currentSb, 'scrapbookId') === element.scrapbookId) {
            patchState({
                currentScrapbook: {
                    ...currentSb,
                    elements: currentSb.elements
                        .map(el => el.elementId === element.elementId ? _.merge(el, element) : el)
                }
            });
        }
    }


    // TODO: REPLACE THIS WITH ABOVE
    @Action(ScrapbookActions.UpdateCurrentScrapbookElement)
    updateCurrentScrapbookElement(
        { patchState, getState }: StateContext<ScrapbookStateModel>,
        { elId, element }: ScrapbookActions.UpdateCurrentScrapbookElement) {

        patchState({
            currentScrapbook: {
                ...getState().currentScrapbook,
                elements: getState().currentScrapbook.elements.map(el => el.elementId === elId ? { ...el, ...element } : el)
            }
        });

        this.scrapbookRemoteService.elementUpdates$.next({ elementId: element.elementId, scrapbookId: element.scrapbookId });
    }
    //#endregion

    constructor(
        private scrapbookRemoteService: ScrapbookRemoteService,
        private matDialog: MatDialog
    ) { }
}

