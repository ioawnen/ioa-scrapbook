import { IScrapbook, IScrapbookBundle } from '../models/scrapbook.model';

export interface ScrapbookStateModel {
    scrapbooks: IScrapbookBundle[];
    currentScrapbook: IScrapbookBundle;
}
