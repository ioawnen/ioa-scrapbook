import { IElement, IElementBundle } from './element.model';
import { IScrapbook, IScrapbookDimensions } from './scrapbook.model';


export class ScrapbookBuilder {
    private _creatorId: string;
    private _title = 'New Scrapbook';
    private _dimensions: IScrapbookDimensions = { width: '500px', height: '500px' };
    private _icon = 'default';
    private _iconBgColor: string;

    get creatorId(): string {
        return this._creatorId;
    }

    get title(): string {
        return this._title;
    }

    get dimensions(): IScrapbookDimensions {
        return this._dimensions;
    }

    get icon(): string {
        return this._icon;
    }

    get iconBgColor(): string {
        return this._iconBgColor;
    }


    constructor() {
        this.setIconBgColor();
    }


    setCreatorId(creatorId: string): this {
        this._creatorId = creatorId;
        return this;
    }

    setTitle(title: string): this {
        this._title = title;
        return this;
    }

    setDimensions(width: string, height: string): this {
        this._dimensions = { width, height };
        return this;
    }

    setIcon(icon: string): this {
        this._icon = icon;
        return this;
    }

    setIconBgColor(color?: string): this {
        this._iconBgColor = color ? color : this.randomColor;
        console.warn('COLOR', this._iconBgColor);
        return this;
    }

    build(): IScrapbook {
        return {
            creatorId: this._creatorId,
            title: this._title,
            dimensions: this._dimensions,
            icon: this._icon,
            iconBgColor: this._iconBgColor
        };
    }

    private get randomColor(): string {
        const letters = '0123456789ABCDEF';
        let color = '#';
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
}
