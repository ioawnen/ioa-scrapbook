import { IElementBundle } from './element.model';


export interface IScrapbookBundle {
    scrapbookId: string;
    scrapbook: IScrapbook;
    elements?: IElementBundle[];

}
export interface IScrapbook {
    creatorId: string;
    title: string;
    dimensions: IScrapbookDimensions;
    iconBgColor: string;
    icon: string;
}

export interface IScrapbookDimensions {
    height: string;
    width: string;
}

