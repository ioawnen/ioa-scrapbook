export interface IElementBundle {
    elementId: string;
    scrapbookId: string;
    element: IElement;
}

export interface IElement {
    type: string;
    order: number;
    position: IElementPosition;
    dimensions: IElementDimensions;
    data: ElementDataType;
}

export interface IElementPosition {
    top: number;
    left: number;
    level: number;
}

export interface IElementDimensions {
    height: number;
    width: number;
}

// tslint:disable-next-line:no-empty-interface
export interface IElementData {

}

export type ElementDataType = ITextboxElementData | IShapeElementData;

export interface ITextboxElementData extends IElementData {
    text: string;
}

export interface IShapeElementData extends IElementData {
    color: string;
}
