import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScrapbookComponent, ScrapbookOverlayComponent, ScrapbookContentComponent } from './components';
import { ScrapbookState } from './store';
import { NgxsModule, Store, Actions, ofActionSuccessful } from '@ngxs/store';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AuthState } from '../auth/store/auth.state';
import { skipWhile, skipUntil, switchMap, filter, buffer, debounceTime } from 'rxjs/operators';
import { ScrapbookRemoteService } from './services/scrapbook-remote.service';
import * as ScrapbookActions from './store/scrapbook.actions';
import { NewScrapbookContentComponent } from './components/new-scrapbook-content/new-scrapbook-content.component';
import { MaterialModule } from '../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ElementFactory } from './components/elements/element-factory/element-factory';
import { TextboxComponent } from './components/elements/textbox/textbox.component';
import { ElementHostDirective } from './components/elements/element-host.directive';
import { LayoutModule } from '../layout/layout.module';
import { ShapeComponent } from './components/elements/shape/shape.component';
import { ScrapbookUXState } from './store/scrapbook-ux/scrapbook-ux.state';
import { ScrapbookSettingsContentComponent } from './components/scrapbook-settings-content/scrapbook-settings-content.component';

@NgModule({
  imports: [
    NgxsModule.forFeature([
      ScrapbookState,
      ScrapbookUXState
    ]),
    CommonModule,
    AngularFirestoreModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule
  ],
  declarations: [
    ScrapbookComponent,
    ScrapbookOverlayComponent,
    ScrapbookContentComponent,
    NewScrapbookContentComponent,
    ElementFactory,
    TextboxComponent,
    ElementHostDirective,
    ShapeComponent,
    ScrapbookSettingsContentComponent
  ],
  providers: [ScrapbookRemoteService],
  exports: [ScrapbookComponent],
  entryComponents: [NewScrapbookContentComponent, TextboxComponent, ShapeComponent]
})
export class ScrapbookModule {

  constructor(private store: Store, actions: Actions, sbRemoteService: ScrapbookRemoteService) {
    store.select(AuthState.isLoggedIn)
      .pipe(
        skipWhile((isloggedIn => !isloggedIn)),
        switchMap(() => sbRemoteService.getScrapbooks())
      ).subscribe((sbs) => {
        store.dispatch(new ScrapbookActions.UpdateScrapbooks(sbs));
      });

    const updateActions = actions.pipe(ofActionSuccessful(ScrapbookActions.UpdateScrapbookElement));
    updateActions.pipe(
      buffer(updateActions.pipe(debounceTime(250)))
    ).subscribe((elements: ScrapbookActions.UpdateScrapbookElement[]) => {
      const sb = this.store.selectSnapshot(ScrapbookState.currentScrapbook);
      elements.forEach(el =>

        // TODO: This doesn't deal with elements that aren't in the current scrapbook
        sbRemoteService.updateScrapbookElement(sb.elements.find(e => e.elementId === el.element.elementId) || null)
      );
    });
  }
}
