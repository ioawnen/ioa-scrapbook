import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';
import { Observable, from, combineLatest, ReplaySubject } from 'rxjs';
import { map, buffer, debounceTime, filter } from 'rxjs/operators';
import { Store, Select } from '@ngxs/store';
import { User } from 'firebase';
import { AuthState } from '../../auth/store/auth.state';
import { IScrapbook, IScrapbookBundle } from '../models/scrapbook.model';
import { ScrapbookBuilder } from '../models/scrapbook.builder';
import * as _ from 'lodash';
import { IElement, IElementBundle } from '../models/element.model';
import { ScrapbookState } from '../store';

@Injectable({
    providedIn: 'root'
})
export class ScrapbookRemoteService {

    public elementUpdates$ = new ReplaySubject<{ scrapbookId: string, elementId: string }>();

    private get collectionName(): string {
        return `scrapbooks`;
    }

    constructor(private db: AngularFirestore, private store: Store) {
    }

    createScrapbook(scrapbook?: IScrapbook): Observable<DocumentReference> {
        return from(
            this.db.collection<IScrapbook>(this.collectionName).add(
                _.toPlainObject(
                    scrapbook ? scrapbook : new ScrapbookBuilder()
                        .setCreatorId(this.store.selectSnapshot(AuthState.firebaseUser).uid).build()
                )
            )
        );
    }

    getScrapbooks(): Observable<IScrapbookBundle[]> {
        return this.db.collection<IScrapbook>(
            this.collectionName,
            r => r.where('creatorId', '==', this.store.selectSnapshot(AuthState.firebaseUser).uid)
        ).snapshotChanges().pipe(
            map(x =>
                x.map(y => ({ scrapbook: y.payload.doc.data(), scrapbookId: y.payload.doc.id }))
            )
        );
    }

    updateScrapbookElement(element: Partial<IElementBundle>) {
        if (element) {
            this.db.collection<IScrapbook>(this.collectionName)
                .doc(element.scrapbookId).collection<IElement>('elements').doc(element.elementId).update(element.element);
        }
    }

    getScrapbook(id: string): Observable<IScrapbookBundle> {
        const sbDoc = this.db.collection<IScrapbook>(this.collectionName).doc<IScrapbook>(id);

        return combineLatest(sbDoc.snapshotChanges(), sbDoc.collection<IElement>('elements').snapshotChanges()).pipe(
            map(([doc, els]) => (<IScrapbookBundle>{
                scrapbookId: doc.payload.id,
                scrapbook: doc.payload.data(),
                elements: els.map(e => ({
                    scrapbookId: doc.payload.id,
                    element: e.payload.doc.data(),
                    elementId: e.payload.doc.id
                })
                )
            }))
        );
    }
}
