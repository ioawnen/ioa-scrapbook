import { TestBed, inject } from '@angular/core/testing';

import { ScrapbookRemoteService } from './scrapbook-remote.service';

describe('ScrapbookRemoteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ScrapbookRemoteService]
    });
  });

  it('should be created', inject([ScrapbookRemoteService], (service: ScrapbookRemoteService) => {
    expect(service).toBeTruthy();
  }));
});
