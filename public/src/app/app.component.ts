import { Component } from '@angular/core';
import { Store } from '@ngxs/store';
import * as AppActions from './store/app.actions';

@Component({
  selector: 'ioa-root',
  templateUrl: './app.component.html',
  styles: []
})
export class AppComponent {

  constructor(store: Store) {
    store.dispatch(new AppActions.SetLoaded());
  }
}
